# [KickStart Interface](https://bitbucket.org/maqinainternet/kickstart-interface)

## Dependencies

- [NodeJS](http://nodejs.org/)
- [Ruby](https://www.ruby-lang.org/) with [SASS](http://sass-lang.com/)
- [Python](https://www.python.org/)

## Development

```
cd /path/to/app
npm install
bower install
grunt prapare
grunt server
```

### Allow network access

```
grunt server --allow-remote
```

## Build

```
grunt build
```

## Recommended

### Editor

[Sublime Text](http://www.sublimetext.com/) with [this](https://bitbucket.org/magnobiet/sublime-text) packages
